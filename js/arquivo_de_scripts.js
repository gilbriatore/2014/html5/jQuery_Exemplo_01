$(function(){

    //Seleção por TAG
    //$("p").css("border","3px solid red");
    //$("li").css("border","3px solid red");
    
    //Seleção por ID
    //$("#item2").css("border","3px solid red");

    //Seleção por ClASS
    //$(".a").css("border","3px solid red");
    //$("li.b").css("border","3px solid green");
    //$("ul .b").css("border","3px solid green");
    //$(".a, .b").css("border","3px solid green");
    
    //Seleciona todos os elementos.
    //$("*").css("border","3px solid green");
    
    
    //$("li:first").css("border","3px solid red");
    //$("li:last").css("border","3px solid red");
    
    //$("li:contains(3), p:contains(3)").css("border","3px solid red");
    
    //$("li:eq(0)").css("border","3px solid red");
    
    //$("li:even").css("border","3px solid red");
    //$("li:odd").css("border","3px solid red");
    
    function mensagem(){
        alert("Botão dinâmico!");           
    }
    
    $("<p/>").text("Parágrafo 4").insertAfter("p:last");
    //$("<button/>").text("Clique-me").on("click",mensagem).insertAfter("p:last");
    
    $("<button/>").text("Clique-me").attr("id","botao").insertAfter("p:last");
    
    $("#botao").on("click",mensagem);
        
    $("body").append(
        $("<button/>")
            .attr("id","botao2")
            .text("Clique-me 2")
            .css("top",300)
            .css("left",300)
            .css("position","absolute")
    );
    
});










/*
function cliqueMe(){
   alert("O link foi clicado!");    
}

//jQuery(document).ready(function(){
//$(document).ready(function(){

$(function(){      
    $("#item").on("click", cliqueMe);
});

*/